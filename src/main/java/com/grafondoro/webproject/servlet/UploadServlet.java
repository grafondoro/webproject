/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.servlet;

import com.grafondoro.webproject.data.DBManager;
import com.grafondoro.webproject.object.Image;
import com.grafondoro.webproject.object.User;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author anase
 */
@WebServlet(name = "UploadServlet", urlPatterns = {"/upload"})
@MultipartConfig
public class UploadServlet extends HttpServlet {

    private DBManager db;
    private final Logger log = Logger.getLogger(NotificationsServlet.class.getName());

    /**
     *
     */
    @Override
    public void init() {
        try {
            db = new DBManager();
        } catch (SQLException | ClassNotFoundException e) {
            log.log(Level.SEVERE, null, e);
        }
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id_restaurant = Integer.parseInt(request.getParameter("id_restaurant"));
        User user = (User) request.getSession().getAttribute("user");
        Part file_part = request.getPart("file");
        String file_name = Paths.get(file_part.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        InputStream input_file = file_part.getInputStream();
        
        String real_path_prefix = getServletContext().getRealPath("/staticfiles");
        String upload_path = String.format("/images/%d/%s", id_restaurant, file_name);
        File destination_file = new File(real_path_prefix, upload_path);
        destination_file.mkdirs();
        destination_file.createNewFile();
        Files.copy(input_file, destination_file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        
        try{
            Image image = new Image();
            image.setLink(String.format("staticfiles%s", upload_path));
            db.insertImage(image, user, id_restaurant);
        } catch (SQLException e) {
            log.log(Level.SEVERE, null, e);
        }
        
        response.sendRedirect(String.format("/WebProject/restaurant?id=%d", id_restaurant));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
