/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.servlet;

import com.grafondoro.webproject.data.DBManager;
import com.grafondoro.webproject.object.Restaurant;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.Collectors.toList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author anase
 */
@WebServlet(name = "SearchServlet", urlPatterns = {"/search", "/search/json"})
public class SearchServlet extends HttpServlet {

    private DBManager db;
    private final Logger log = Logger.getLogger(NotificationsServlet.class.getName());

    /**
     *
     */
    @Override
    public void init() {
        try {
            db = new DBManager();
        } catch (SQLException | ClassNotFoundException e) {
            log.log(Level.SEVERE, null, e);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //used pattern search or search/json in our case
        String pattern = request.getServletPath();

        String location = request.getParameter("location");
        String hint = request.getParameter("hint").toLowerCase();
        String sort = request.getParameter("sort");
        String order = request.getParameter("order");
        String requested_page = request.getParameter("page");
        String[] cuisines_filter = request.getParameterValues("cuisines");
        List<String> cuisines = cuisines_filter == null ? new ArrayList<>() : Arrays.asList(cuisines_filter);

        if (sort == null) {
            sort = "none";
        }

        if (requested_page == null) {
            requested_page = "1";
        }

        int page = Integer.parseInt(requested_page);

        List<String> orders = Arrays.asList("desc", "asc");

        if (!orders.contains(order)) {
            order = "desc";
        }

        if (sort == null) {
            sort = "none";
        }

        List<Restaurant> restaurants = new ArrayList<>();
        int restaurants_count = 0;

        try {
            restaurants = db.searchRestaurants(hint, location, page, cuisines);
            restaurants_count = restaurants.size();

            switch (sort) {
                case "name":

                    Comparator<Restaurant> byName = (r1, r2) -> r1.getName().compareTo(r2.getName());
                    if (order.equals("desc")) {
                        restaurants.sort(byName.reversed());
                    } else {
                        restaurants.sort(byName);
                    }

                    break;
                case "price":

                    Comparator<Restaurant> byPrice = (r1, r2) -> r1.getAverage_prices().getAverage().compareTo(r2.getAverage_prices().getAverage());
                    if (order.equals("desc")) {
                        restaurants.sort(byPrice.reversed());
                    } else {
                        restaurants.sort(byPrice);
                    }

                    break;
                case "reputation":
                default:

                    Comparator<Restaurant> byRating = (r1, r2) -> r1.getAvgRating().compareTo(r2.getAvgRating());
                    if (order.equals("desc")) {
                        restaurants.sort(byRating.reversed());
                    } else {
                        restaurants.sort(byRating);
                    }

                    break;
            }

            restaurants_count = restaurants.size();

            //check legal page
            int starting_element_index = 0;

            if ((page - 1) * 10 < restaurants_count && page > 0) {
                starting_element_index = (page - 1) * 10;
            } else {
                page = 1;
            }
            restaurants = restaurants.stream()
                    .skip(starting_element_index)
                    .limit(10)
                    .collect(toList());
        } catch (SQLException e) {
            log.log(Level.SEVERE, null, e);
        }

        if (pattern.contains("json")) {
            response.setContentType("application/json");

            JSONArray array_builder = new JSONArray();

            for (Restaurant r : restaurants) {
                JSONArray cuisinesJsonArray = new JSONArray();

                r.getCuisines().forEach((cuisine) -> {
                    cuisinesJsonArray.put(cuisine);
                });

                JSONObject object_builder = new JSONObject()
                        .put("name", r.getName())
                        .put("link", r.getLink())
                        .put("image", r.getCover_image())
                        .put("average_rating", r.getAvgRating())
                        .put("prices", new JSONObject()
                                .put("high", r.getAverage_prices().getHigh())
                                .put("low", r.getAverage_prices().getLow()))
                        .put("average_price", r.getAverage_prices().getAverage())
                        .put("reviews_count", r.getReviews().size())
                        .put("cuisines", cuisinesJsonArray);

                array_builder.put(object_builder);
            }

            JSONObject resp = new JSONObject()
                    .put("total", restaurants_count)
                    .put("results", restaurants.size())
                    .put("page", page)
                    .put("restaurants", array_builder);

            response.getWriter().write(resp.toString(4));
        } else {
            RequestDispatcher request_dispatcher = request.getRequestDispatcher("/search.jsp");

            request.setAttribute("restaurants", restaurants);
            request.setAttribute("pages", restaurants_count / 10);
            request.setAttribute("page", page);
            request.setAttribute("sort", sort);
            request.setAttribute("location", location);
            request.setAttribute("hint", hint);

            request_dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
