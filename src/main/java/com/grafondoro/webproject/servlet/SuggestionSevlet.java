/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.servlet;

import com.grafondoro.webproject.data.DBManager;
import com.grafondoro.webproject.object.Suggestion;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.Collectors.toList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author anase
 */
@WebServlet(name = "SuggestionSevlet", urlPatterns = {"/suggestion"})
public class SuggestionSevlet extends HttpServlet {

    private DBManager db;
    private final Logger log = Logger.getLogger(NotificationsServlet.class.getName());

    /**
     *
     */
    @Override
    public void init() {
        try {
            db = new DBManager();
        } catch (SQLException | ClassNotFoundException e) {
            log.log(Level.SEVERE, null, e);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String hint = request.getParameter("hint").toLowerCase();

        //Logger.getLogger(RestaurantServlet.class.getName()).log(Level.INFO, String.format("got request with hint -> %s", hint));
        List<Suggestion> suggestions = new ArrayList<>();

        try {
            suggestions = db.getSuggestions(hint);

            //limit suggestions to 8 always
            //if there's 4 items that are not restauarants show them
            if (suggestions.size() > 8) {
                long not_restaurant_count = suggestions.stream()
                        .filter(s -> !s.getDescription().equals("Ristorante"))
                        .count();
                List<Suggestion> new_suggestions = new ArrayList<>();

                new_suggestions.addAll(suggestions.stream()
                        .filter(s -> !s.getDescription().equals("Ristorante"))
                        .limit(4)
                        .collect(toList()));

                new_suggestions.addAll(suggestions.stream()
                        .filter(s -> s.getDescription().equals("Ristorante"))
                        .limit(8 - suggestions.stream()
                                .filter(s -> !s.getDescription().equals("Ristorante"))
                                .count())
                        .collect(toList()));

                suggestions = new_suggestions;
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, null, e);
        }

        JSONArray array = new JSONArray();

        suggestions.forEach(s -> array.put(new JSONObject()
                .put("content", s.getContent())
                .put("description", s.getDescription())
                .put("link", s.getLink())));

        JSONObject resp = new JSONObject()
                .put("results", suggestions.size())
                .put("suggestions", array);

        response.setContentType("application/json");
        response.getWriter().write(resp.toString(4));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
