/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.servlet;

import com.grafondoro.webproject.data.DBManager;
import com.grafondoro.webproject.object.Notification;
import com.grafondoro.webproject.object.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author anase
 */
@WebServlet(name = "NotificationsServlet", urlPatterns = {"/notifications/list", "/notifications/count"})
public class NotificationsServlet extends HttpServlet {

    private DBManager db;
    private final Logger log = Logger.getLogger(NotificationsServlet.class.getName());

    /**
     *
     */
    @Override
    public void init() {
        try {
            db = new DBManager();
        } catch (SQLException | ClassNotFoundException e) {
            log.log(Level.WARNING, null, e);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pattern = request.getServletPath();

        User user = (User) request.getSession().getAttribute("user");
        List<Notification> notifications = new ArrayList<>();
        int notifications_count = 0;

        try {
            if (user != null) {
                notifications_count = db.getNotificationsCount(user);
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, null, e);
        }

        if (pattern.contains("list")) {
            RequestDispatcher request_dispatcher = request.getRequestDispatcher("/notifications.jsp");

            try {
                notifications = db.getNotifications(user);
            } catch (SQLException e) {
                log.log(Level.SEVERE, null, e);
            }

            request.setAttribute("notifications", notifications);
            request.setAttribute("notifications_count", notifications_count);

            request_dispatcher.forward(request, response);
        } else {
            response.setContentType("application/json");

            JSONObject resp = new JSONObject()
                    .put("count", notifications_count);

            response.getWriter().write(resp.toString(4));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
