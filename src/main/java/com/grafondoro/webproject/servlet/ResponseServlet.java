/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.servlet;

import com.grafondoro.webproject.data.DBManager;
import com.grafondoro.webproject.object.Restaurant;
import com.grafondoro.webproject.object.Review;
import com.grafondoro.webproject.object.User;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anase
 */
@WebServlet(name = "ResponseServlet", urlPatterns = {"/response"})
public class ResponseServlet extends HttpServlet {

    private DBManager db;
    private final Logger log = Logger.getLogger(NotificationsServlet.class.getName());

    /**
     *
     */
    @Override
    public void init() {
        try {
            db = new DBManager();
        } catch (SQLException | ClassNotFoundException e) {
            log.log(Level.SEVERE, null, e);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher request_dispatcher = request.getRequestDispatcher("/review.jsp");

        String review_response = request.getParameter("response");
        int id_review = Integer.parseInt(request.getParameter("id_review"));
        int id_restaurant = Integer.parseInt(request.getParameter("id_restaurant"));
        
        Restaurant restaurant = new Restaurant();
        User user = (User) request.getSession().getAttribute("user");

        Review review = new Review();

        try {
            review = db.getReview(id_review);
            review.setResponse(review_response);
            db.addResponse(review);
            restaurant = db.getRestaurant(id_restaurant);
            request.setAttribute("restaurant", restaurant);
            request.setAttribute("review", review);
        } catch (SQLException e) {
            log.log(Level.SEVERE, null, e);
        }

        request_dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
