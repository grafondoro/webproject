/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.data;

import com.grafondoro.webproject.object.AveragePrices;
import com.grafondoro.webproject.object.BusinessHours;
import com.grafondoro.webproject.object.Coordinates;
import com.grafondoro.webproject.object.Day;
import com.grafondoro.webproject.object.Image;
import com.grafondoro.webproject.object.Location;
import com.grafondoro.webproject.object.Notification;
import com.grafondoro.webproject.object.Restaurant;
import com.grafondoro.webproject.object.Review;
import com.grafondoro.webproject.object.User;
import com.grafondoro.webproject.object.Suggestion;
import com.mysql.cj.jdbc.Driver;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.stream.Collectors.toList;

/**
 *
 * @author anase
 */
public class DBManager {

    private final Connection conn;

    public DBManager() throws SQLException, ClassNotFoundException {
        Driver d = new Driver();
        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/webproject?useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root");
    }

    public void updateImageStatus(Image image) throws SQLException {
        String image_update = "update images set status = ? "
                + "where photo_url = ?";

        PreparedStatement prepared_statement = conn.prepareStatement(image_update);
        prepared_statement.setInt(1, image.getStatus());
        prepared_statement.setString(2, image.getLink());

        prepared_statement.executeUpdate();
    }

    public void insertImage(Image image, User loader, int id_restaurant) throws SQLException {
        String image_insert = "insert into images (photo_url, id_restaurant, id_user) "
                + "values (?, ?, ?)";

        PreparedStatement prepared_statemant = conn.prepareStatement(image_insert);
        prepared_statemant.setString(1, image.getLink());
        prepared_statemant.setInt(2, id_restaurant);
        prepared_statemant.setInt(3, loader.getId());

        prepared_statemant.executeUpdate();
    }

    public User registerUser(String email, String password, String name, String surname) throws SQLException {
        User user = new User();

        String user_insert = "insert into users (name, surname, email, pwd, clear_pwd, is_admin) "
                + "values (?, ?, ?, password(?), ?, 0)";

        PreparedStatement prepared_statement = conn.prepareStatement(user_insert, Statement.RETURN_GENERATED_KEYS);
        prepared_statement.setString(1, name);
        prepared_statement.setString(2, surname);
        prepared_statement.setString(3, email);
        prepared_statement.setString(4, password);
        prepared_statement.setString(5, password);

        prepared_statement.executeUpdate();

        ResultSet result_set = prepared_statement.getGeneratedKeys();

        if (result_set.next()) {
            user = getUser(result_set.getInt(1));
        }

        return user;
    }

    public List<Image> getUserImages(User user) throws SQLException {
        List<Image> images = new ArrayList<Image>();

        String images_select = "select photo_url "
                + "from images "
                + "where id_user = ?";

        PreparedStatement prepared_statemant = conn.prepareStatement(images_select);
        prepared_statemant.setInt(1, user.getId());

        ResultSet result_set = prepared_statemant.executeQuery();

        while (result_set.next()) {
            String photo_url = result_set.getString(1);
            Image image = getImage(photo_url);

            images.add(image);
        }

        return images;
    }

    public Image getImage(String photo_url) throws SQLException {
        Image image = new Image();

        String image_select = "select * "
                + "from images "
                + "where photo_url = ?";

        PreparedStatement prepared_statemant = conn.prepareStatement(image_select);
        prepared_statemant.setString(1, photo_url);

        ResultSet result_set = prepared_statemant.executeQuery();

        while (result_set.next()) {
            int id_user = result_set.getInt("id_user");
            int status = result_set.getInt("status");

            image.setLoader(getUser(id_user));
            image.setStatus(status);
            image.setLink(photo_url);
        }

        return image;
    }

    public List<Restaurant> getUserRestaurants(User user) throws SQLException {
        List<Restaurant> restaurants = new ArrayList<>();

        String restaurants_select = "select id_restaurant "
                + "from restaurants "
                + "where id_user = ?";

        PreparedStatement prepared_statemant = conn.prepareStatement(restaurants_select);
        prepared_statemant.setInt(1, user.getId());

        ResultSet result_set = prepared_statemant.executeQuery();

        while (result_set.next()) {
            int id_restaurant = result_set.getInt(1);
            Restaurant restaurant = getRestaurant(id_restaurant);

            restaurants.add(restaurant);
        }

        return restaurants;
    }

    public List<Restaurant> searchRestaurants(String hint, String location, int page, List<String> cuisines) throws SQLException {
        List<Restaurant> restaurants = new ArrayList<>();

        List<String> locations = Arrays.asList("country", "region", "province", "city");

        if (locations.contains(location)) {

            String restaurants_select = String.format("select id_restaurant "
                    + "from restaurants "
                    + "where lower(%s) = ?", location);

            if (!cuisines.isEmpty()) {
                cuisines = cuisines.stream().map(c -> c.toLowerCase()).collect(toList());

                StringBuilder builder = new StringBuilder();
                cuisines.forEach(c -> builder.append("?, "));

                restaurants_select = String.format("select id_restaurant "
                        + "from restaurants R "
                        + "where lower(%s) like ? "
                        + "and id_restaurant in "
                        + "(select id_restaurant "
                        + "from restaurants_cuisines RC "
                        + "where lower(id_cuisine) in (%s) "
                        + "and R.id_restaurant = RC.id_restaurant "
                        + "group by id_restaurant "
                        + "having count(RC.id_restaurant) = ?)", location,
                        builder.substring(0, 3 * (cuisines.size()) - 2));
            }

            PreparedStatement prepared_statemant = conn.prepareStatement(restaurants_select);
            prepared_statemant.setString(1, hint);
            if (cuisines.size() > 0) {
                int index = 1;
                for (String cuisine : cuisines) {
                    prepared_statemant.setString(++index, cuisine);
                }

                prepared_statemant.setInt(++index, cuisines.size());
            }

            ResultSet result_set = prepared_statemant.executeQuery();

            while (result_set.next()) {
                int id_restaurant = result_set.getInt(1);
                Restaurant restaurant = getRestaurant(id_restaurant);

                restaurants.add(restaurant);
            }
        }

        return restaurants;
    }

    public void claimRestaurant(User user, int id_restaurant) throws SQLException {
        String restaurant_update = "update restaurants set id_user = ? "
                + "where id_restaurant = ? "
                + "and id_user is null";

        PreparedStatement prepared_statement = conn.prepareStatement(restaurant_update);
        prepared_statement.setInt(1, user.getId());
        prepared_statement.setInt(2, id_restaurant);

        prepared_statement.executeUpdate();
    }

    public Restaurant getRestaurant(int id_restaurant) throws SQLException {
        Restaurant restaurant = new Restaurant();
        restaurant.setId(id_restaurant);

        String restaurant_select = "select * "
                + "from restaurants "
                + "where id_restaurant = ?";

        PreparedStatement prepared_statement = conn.prepareStatement(restaurant_select);
        prepared_statement.setInt(1, id_restaurant);

        ResultSet result_set = prepared_statement.executeQuery();

        int owner_id = 0;

        if (result_set.next()) {
            String name = result_set.getString("name");
            String description = result_set.getString("description");
            String website = result_set.getString("website");

            restaurant.setName(name);
            restaurant.setDescription(description);
            restaurant.setWebsite(website);

            String country = result_set.getString("country");
            String region = result_set.getString("region");
            String province = result_set.getString("province");
            String city = result_set.getString("city");
            String street_address = result_set.getString("street_address");
            float latitude = result_set.getFloat("latitude");
            float longitude = result_set.getFloat("longitude");

            Coordinates coordinates = new Coordinates();
            coordinates.setLatitude(latitude);
            coordinates.setLongitude(longitude);

            Location location = new Location();
            location.setCountry(country);
            location.setRegion(region);
            location.setProvince(province);
            location.setCity(city);
            location.setStreet_address(street_address);
            location.setCoordinates(coordinates);

            restaurant.setLocation(location);

            int high_price = result_set.getInt("high_price");
            int low_price = result_set.getInt("low_price");

            AveragePrices average_prices = new AveragePrices();
            average_prices.setHigh(high_price);
            average_prices.setLow(low_price);

            restaurant.setAverage_prices(average_prices);

            owner_id = result_set.getInt("id_user");
        }

        if (owner_id != 0) {
            restaurant.setOwner(getUser(owner_id));
        }

        List<String> cuisines = new ArrayList<>();

        String cuisines_select = "select * "
                + "from restaurants_cuisines "
                + "where id_restaurant = ?";

        prepared_statement = conn.prepareStatement(cuisines_select);
        prepared_statement.setInt(1, id_restaurant);

        result_set = prepared_statement.executeQuery();

        while (result_set.next()) {
            String cuisine = result_set.getString("id_cuisine");

            cuisines.add(cuisine);
        }

        restaurant.setCuisines(cuisines);

        restaurant.setReviews(getReviews(id_restaurant));

        List<Image> images = new ArrayList<>();

        String images_select = "select * "
                + "from images "
                + "where id_restaurant = ?";

        prepared_statement = conn.prepareStatement(images_select);
        prepared_statement.setInt(1, id_restaurant);

        result_set = prepared_statement.executeQuery();

        while (result_set.next()) {
            String photo_url = result_set.getString("photo_url");

            Image img = getImage(photo_url);

            images.add(img);
        }

        restaurant.setImages(images);

        Optional<Image> img = images.stream()
                .filter(i -> i.getStatus() == 0)
                .findFirst();

        String cover_image_link = img.isPresent() ? img.get().getLink() : "no_image";

        restaurant.setCover_image(cover_image_link);

        List<BusinessHours> business_hourses = new ArrayList<>();

        String business_hours_select = "select * "
                + "from restaurants_business_hours "
                + "where id_restaurant = ?";

        prepared_statement = conn.prepareStatement(business_hours_select);
        prepared_statement.setInt(1, id_restaurant);

        result_set = prepared_statement.executeQuery();

        while (result_set.next()) {
            Day day = getDay(result_set.getString("day"));
            LocalTime opens = result_set.getTime("opens").toLocalTime();
            LocalTime closes = result_set.getTime("closes").toLocalTime();

            BusinessHours businessHours = new BusinessHours();
            businessHours.setDay(day);
            businessHours.setOpens(opens);
            businessHours.setCloses(closes);

            business_hourses.add(businessHours);
        }

        business_hourses.sort((bh1, bh2) -> bh1.getDay().getDayNumber().compareTo(bh2.getDay().getDayNumber()));

        restaurant.setBusiness_hours(business_hourses);

        return restaurant;
    }

    public Day getDay(String name) {
        Map<String, Day> days = new HashMap<>();
        days.put("lunedì", Day.MONDAY);
        days.put("martedì", Day.TUESDAY);
        days.put("mercoledì", Day.WEDNESDAY);
        days.put("giovedì", Day.THURSDAY);
        days.put("venerdì", Day.FRIDAY);
        days.put("sabato", Day.SATURDAY);
        days.put("domenica", Day.SUNDAY);

        return days.get(name);
    }

    public List<Review> getReviews(int id_restaurant) throws SQLException {
        List<Review> reviews = new ArrayList<>();

        String reviews_select = "select id_review "
                + "from reviews "
                + "natural join users "
                + "where id_restaurant = ? ";

        PreparedStatement prepared_statement = conn.prepareStatement(reviews_select);
        prepared_statement.setInt(1, id_restaurant);

        ResultSet result_set = prepared_statement.executeQuery();

        while (result_set.next()) {
            int id_review = result_set.getInt(1);

            Review review = getReview(id_review);

            reviews.add(review);
        }

        return reviews;
    }

    public Review getReview(int id_review) throws SQLException {
        Review review = new Review();

        String review_select = "select * "
                + "from reviews "
                + "where id_review = ?";

        PreparedStatement prepared_statement = conn.prepareStatement(review_select);
        prepared_statement.setInt(1, id_review);

        ResultSet result_set = prepared_statement.executeQuery();

        if (result_set.next()) {
            int rating = result_set.getInt("rating");
            int id_user = result_set.getInt("id_user");
            String content = result_set.getString("content");
            String title = result_set.getString("title");

            User reviewer = getUser(id_user);

            review.setId(id_review);
            review.setContent(content);
            review.setTitle(title);
            review.setRating(rating);
            review.setReviewer(reviewer);
        }

        return review;
    }

    public int authenticate(String email, String password) throws SQLException {
        String restaurant_select = "select id_user "
                + "from users "
                + "where email = ? "
                + "and password(?) = pwd";

        PreparedStatement prepared_statement = conn.prepareStatement(restaurant_select);
        prepared_statement.setString(1, email);
        prepared_statement.setString(2, password);

        ResultSet result_set = prepared_statement.executeQuery();

        if (result_set.next()) {
            return result_set.getInt(1);
        }

        return 0;
    }

    public User getUser(int id_user) throws SQLException {
        User user = new User();

        String user_select = "select * "
                + "from users "
                + "where id_user = ?";

        PreparedStatement prepared_statement = conn.prepareStatement(user_select);
        prepared_statement.setInt(1, id_user);

        ResultSet result_set = prepared_statement.executeQuery();

        if (result_set.next()) {
            String email = result_set.getString("email");
            String name = result_set.getString("name");
            String surname = result_set.getString("surname");
            int is_admin = result_set.getInt("is_admin");

            user.setAdmin(is_admin == 1);
            user.setEmail(email);
            user.setName(name);
            user.setSurname(surname);
            user.setId(id_user);
        }

        return user;
    }

    public boolean checkDuplicateEmail(String email) throws SQLException {
        String restaurant_select = "select * "
                + "from users "
                + "where email = ?";

        PreparedStatement prepared_statement = conn.prepareStatement(restaurant_select);
        prepared_statement.setString(1, email);

        ResultSet result_set = prepared_statement.executeQuery();

        boolean duplicate = result_set.isBeforeFirst();

        return duplicate;
    }

    public List<Suggestion> getSuggestions(String hint) throws SQLException {
        List<Suggestion> suggestions = new ArrayList<>();

        String restaurants_select = "select id_restaurant, name "
                + "from restaurants "
                + "where lower(name) like ? ";

        PreparedStatement prepared_statemant = conn.prepareStatement(restaurants_select);
        prepared_statemant.setString(1, String.format("%%%s%%", hint.toLowerCase()));

        ResultSet result_set = prepared_statemant.executeQuery();

        while (result_set.next()) {
            String content = result_set.getString("name");
            String description = "Ristorante";
            int id_restaurant = result_set.getInt("id_restaurant");

            Restaurant restaurant = new Restaurant();
            restaurant.setId(id_restaurant);

            String link = restaurant.getLink();

            Suggestion suggestion = new Suggestion();
            suggestion.setContent(content);
            suggestion.setDescription(description);
            suggestion.setLink(link);

            suggestions.add(suggestion);
        }

        List<String> locations = Arrays.asList("country", "region", "province", "city");

        for (String location : locations) {
            String location_select = String.format("select %1$s "
                    + "from restaurants "
                    + "where lower(%1$s) like ? "
                    + "group by %1$s", location);

            prepared_statemant = conn.prepareStatement(location_select);
            prepared_statemant.setString(1, String.format("%%%s%%", hint));

            result_set = prepared_statemant.executeQuery();

            while (result_set.next()) {
                String content = result_set.getString(1);
                String description = getItalianDescription(location);

                String content_encoded = "nothing";

                try {
                    content_encoded = URLEncoder.encode(content, "UTF-8");
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
                }

                String link = String.format("/WebProject/search?location=%s&hint=%s",
                        location, content_encoded);

                Suggestion suggestion = new Suggestion();
                suggestion.setContent(content);
                suggestion.setDescription(description);
                suggestion.setLink(link);

                suggestions.add(suggestion);
            }
        }

        return suggestions;
    }

    public String getItalianDescription(String english_version) {
        String description;

        switch (english_version) {
            case "country":
                description = "Nazione";
                break;
            case "region":
                description = "Regione";
                break;
            case "province":
                description = "Provincia";
                break;
            case "city":
                description = "Città";
                break;
            default:
                description = "Sconosciuto";
                break;
        }

        return description;
    }

    public int getNotificationsCount(User user) throws SQLException {
        int notifications_count = 0;

        String notifications_select = "select count(*) "
                + "from notifications "
                + "natural join users "
                + "where id_user = ? "
                + "and notification_time > last_notification_seen";

        PreparedStatement prepared_statement = conn.prepareStatement(notifications_select);
        prepared_statement.setInt(1, user.getId());

        ResultSet result_set = prepared_statement.executeQuery();

        if (result_set.next()) {
            notifications_count = result_set.getInt(1);
        }

        return notifications_count;
    }

    public List<Notification> getNotifications(User user) throws SQLException {
        List<Notification> notifications = new ArrayList<>();

        String notifications_select = "select * "
                + "from notifications "
                + "where id_user = ? "
                + "order by notification_time desc";

        PreparedStatement prepared_statement = conn.prepareStatement(notifications_select);
        prepared_statement.setInt(1, user.getId());

        ResultSet result_set = prepared_statement.executeQuery();

        while (result_set.next()) {
            String content = result_set.getString("content");
            String link = result_set.getString("link");
            Date notification_time = result_set.getTimestamp("notification_time");

            Notification notification = new Notification();
            notification.setContent(content);
            notification.setLink(link);
            notification.setNotification_time(notification_time);
            notification.setUser(user);

            notifications.add(notification);
        }

        String last_seen_update = "update users set last_notification_seen = now() "
                + "where id_user = ?";

        prepared_statement = conn.prepareStatement(last_seen_update);
        prepared_statement.setInt(1, user.getId());

        prepared_statement.executeUpdate();

        return notifications;
    }

    public Restaurant getReviewRestaurant(Review review) throws SQLException {
        Restaurant restaurant = new Restaurant();

        String review_select = "select id_restaurant "
                + "from reviews "
                + "where id_review = ?";

        PreparedStatement prepapred_statement = conn.prepareStatement(review_select);
        prepapred_statement.setInt(1, review.getId());

        ResultSet result_set = prepapred_statement.executeQuery();

        if (result_set.next()) {
            int id_restaurant = result_set.getInt(1);
            restaurant = getRestaurant(id_restaurant);
        }

        return restaurant;
    }

    public Review addReview(Review review, User user, int id_restaurant) throws SQLException {
        String review_insert = "insert into reviews (id_user, id_restaurant, title, content, rating) "
                + "values (?, ?, ?, ?, ?)";

        PreparedStatement prepared_statement = conn.prepareStatement(review_insert, Statement.RETURN_GENERATED_KEYS);
        prepared_statement.setInt(1, user.getId());
        prepared_statement.setInt(2, id_restaurant);
        prepared_statement.setString(3, review.getTitle());
        prepared_statement.setString(4, review.getContent());
        prepared_statement.setInt(5, review.getRating());

        prepared_statement.executeUpdate();

        ResultSet result_set = prepared_statement.getGeneratedKeys();

        if (result_set.next()) {
            review = getReview(result_set.getInt(1));
        }

        return review;
    }

    public void addResponse(Review review) throws SQLException {
        String review_update = "update reviews set response = ? "
                + "where id_review = ?";

        PreparedStatement prepared_statement = conn.prepareCall(review_update);
        prepared_statement.setString(1, review.getResponse());
        prepared_statement.setInt(2, review.getId());

        prepared_statement.executeUpdate();
    }
}
