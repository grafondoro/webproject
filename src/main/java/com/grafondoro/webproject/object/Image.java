/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.object;

/**
 *
 * @author anase
 */
public class Image {

    private User loader;
    private String link;
    private int status;

    public String getStatusString() {
        String retval = "";
        
        switch(this.status){
            case 0:
                retval = "okay";
                break;
            case 1:
                retval = "segnalata";
                break;
            case 2:
                retval = "cancellata";
                break;
            default:
                break;
        }
        
        return retval;
    }

    public User getLoader() {
        return loader;
    }

    public void setLoader(User loader) {
        this.loader = loader;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
