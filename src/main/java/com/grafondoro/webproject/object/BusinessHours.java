/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.object;

import java.time.LocalTime;

/**
 *
 * @author anase
 */
public class BusinessHours {
    
    private Day day;
    private LocalTime opens;
    private LocalTime closes;

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public LocalTime getOpens() {
        return opens;
    }

    public void setOpens(LocalTime opens) {
        this.opens = opens;
    }

    public LocalTime getCloses() {
        return closes;
    }

    public void setCloses(LocalTime closes) {
        this.closes = closes;
    }
}
