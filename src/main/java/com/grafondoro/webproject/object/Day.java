/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.object;

/**
 *
 * @author anase
 */
public enum Day {

    MONDAY("lunedì", 1),
    TUESDAY("martedì", 2),
    WEDNESDAY("mercoledì", 3),
    THURSDAY("giovedì", 4),
    FRIDAY("venerdì", 5),
    SATURDAY("sabato", 6),
    SUNDAY("domenica", 7);

    private final String name;
    private final int number;

    Day(String name, int number) {
        this.name = name;
        this.number = number;
    }

    public Integer getDayNumber() {
        return number;
    }

    public String getDayName() {
        return name;
    }
}
