/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grafondoro.webproject.object;

import java.util.List;

/**
 *
 * @author anase
 */
public class Restaurant {

    private int id;
    private String name;
    private String website;
    private String description;
    private String cover_image;
    private List<String> cuisines;
    private List<Image> images;
    private List<BusinessHours> business_hours;
    private List<Review> reviews;
    private AveragePrices average_prices;
    private Location location;
    private User owner;

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink(){
        return String.format("/WebProject/restaurant?id=%d", id);
    }
    
    public Double getAvgRating() {
        return (double) Math.round(reviews.stream()
                .mapToDouble(r -> r.getRating())
                .average()
                .orElse(Double.NaN) * 100.0) / 100.0;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getCuisines() {
        return cuisines;
    }

    public void setCuisines(List<String> cuisines) {
        this.cuisines = cuisines;
    }

    public List<BusinessHours> getBusiness_hours() {
        return business_hours;
    }

    public void setBusiness_hours(List<BusinessHours> business_hours) {
        this.business_hours = business_hours;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public AveragePrices getAverage_prices() {
        return average_prices;
    }

    public void setAverage_prices(AveragePrices average_prices) {
        this.average_prices = average_prices;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
