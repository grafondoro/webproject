<%-- 
    Document   : register
    Created on : Jan 15, 2017, 4:15:41 PM
    Author     : anase
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Page</title>
        <link rel="stylesheet" href="staticfiles/css/login_register.css">
        <%@include file="base_head.jsp" %>
        <script src="staticfiles/js/register.js"></script>
    </head>
    <body>
        <div class="container">
            <form class="form-signin form-register" action="register" id="myForm" method="post">
                <h2 class="form-signin-heading">Registrati</h2>
                <label for="inputName" class="sr-only">Nome</label>
                <input type="text" id="inputName" name="name" class="form-control" placeholder="Nome" autofocus/>
                <div hidden class="alert alert-danger name">
                    Campo "Nome" non compilato
                </div>
                <label for="inputSurname" class="sr-only">Cognome</label>
                <input type="text" id="inputSurname" name="surname" class="form-control" placeholder="Cognome" autofocus/>
                <div hidden class="alert alert-danger surname">
                    Campo "Cognome" non compilato
                </div>
                <label for="inputEmail" class="sr-only">Indirizzo email</label>
                <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Indirizzo email" autofocus/>
                <div hidden class="alert alert-danger email-duplicate">
                    Esiste già un utente registrato con questa email
                </div>
                <div hidden class="alert alert-danger email-valid">
                    Campo "Email" non valido
                </div>
                <div hidden class="alert alert-danger email-empty">
                    Campo "Email" non compilato
                </div>
                <label for="repeatInputEmail" class="sr-only">Ripeti indirizzo Email</label>
                <input type="email" id="repeatInputEmail" class="form-control" placeholder="Ripetere indirizzo email" autofocus/>
                <div hidden class="alert alert-danger repeat-email">
                    Le due email che hai scritto non matchano
                </div>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password"/>
                <div hidden class="alert alert-danger password">
                    Campo "Password" non compilato
                </div>
                <label for="repeatInputPassword" class="sr-only">Ripeti password</label>
                <input type="password" id="repeatInputPassword" class="form-control" placeholder="Ripeti password"/>
                <div hidden class="alert alert-danger repeat-password">
                    Le due password che hai scritto non matchano
                </div>
                <input type="checkbox" id="conditions" name="conditions" value="accepted">Accetti la normativa sulla privacy<br>
                <div hidden class="alert alert-danger checkbox-tocheck">
                    Accettare le condizioni di utilizzo
                </div>
                <button class="btn btn-primary btn-block" name="submitted" id="submitted" type="submit" disabled="disabled">Registrati</button>
                <a class="btn btn-default btn-block" href="<%=request.getHeader("Referer")%>">Annulla</a>
            </form>
        </div>
    </body>
</html>
