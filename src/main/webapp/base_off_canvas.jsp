<%-- 
    Document   : layout_off_canvas
    Created on : Feb 1, 2017, 10:58:24 AM
    Author     : anase
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<div id="drawer" class="panel panel-default off-canvas">
    <div class="panel-heading">
        Menu
    </div>
    <div class="panel-body">
        <div class="list-group">
            <c:choose>
                <c:when test="${!empty sessionScope.user}">
                    <a class="list-group-item off-canvas-item" href="/WebProject/myuser">mio profilo</a>
                    <a class="list-group-item off-canvas-item" id="notifications" href="/WebProject/notifications/list">notifiche</a>
                    <a class="list-group-item off-canvas-item" href="/WebProject/logout">logout</a>
                </c:when>
                <c:otherwise>
                    <a class="list-group-item off-canvas-item" href="/WebProject/login.jsp">login</a>
                    <a class="list-group-item off-canvas-item" href="/WebProject/register.jsp">registrati</a>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>