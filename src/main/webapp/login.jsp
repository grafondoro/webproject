<%-- 
    Document   : login
    Created on : Jan 15, 2017, 12:13:03 PM
    Author     : anase
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="staticfiles/css/login_register.css">
        <%@include file="base_head.jsp" %>
    </head>
    <body>
        <div class="container">

            <form class="form-signin" action="login" method="post">
                <h2 class="form-signin-heading">Effettua l'accesso</h2>
                <label for="inputEmail" class="sr-only">Indirizzo email</label>
                <input type="email" name="email" id="inputEmail" value="${email}" class="form-control" placeholder="Email address" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                <div <c:if test="${empty error}">hidden</c:if> class="alert alert-danger">
                    Mail e/o password errati
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Accedi</button>
                <a class="btn btn-default btn-block" href="<%=request.getHeader("Referer")%>">Annulla</a>
            </form>
        </div>
    </body>
</html>
