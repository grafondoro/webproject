<%-- 
    Document   : index
    Created on : Jan 23, 2017, 11:13:48 AM
    Author     : anase
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="base_head.jsp" %>
    </head>
    <body>
        <%@include file="base_off_canvas.jsp" %>
        <main class="main-view">
            <%@include file="base_navbar.jsp" %>
            <div class="jumbotron">
                <h1>Benvenuto!</h1>

                <p><a class="btn btn-primary btn-lg" href="https://docs.google.com/document/d/1U39DdFC-huEU_Dwue5RsXtMd499hHXBfNkNdO5R3sE4/edit?usp=sharing"> </a></p>
                <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
                <p><a class="btn btn-primary btn-lg" href="/WebProject/restaurant?id=1">restaurant 1</a></p>
            
            </div>
        </main>
    </body>
</html>
