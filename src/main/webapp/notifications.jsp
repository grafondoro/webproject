<%-- 
    Document   : notifications
    Created on : 1-feb-2017, 17.14.13
    Author     : Nauman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
    <head>
        <title>Notifications Page</title>
        <%@include file="base_head.jsp" %>
        <link rel="stylesheet" href="/WebProject/staticfiles/css/notifications.css">
    </head>
    <body>
        <%@include file="base_off_canvas.jsp" %>
        <main class="main-view">
            <%@include file="base_navbar.jsp" %>
            <div class="list-group">
                <c:forEach items="${notifications}" var="notification" varStatus="i">
                    <a class="
                       list-group-item
                       <c:if test="${i.index < notifications_count}">
                           list-group-item-info
                       </c:if>
                       " href="${notification.link}">${notification.content}</a>
                </c:forEach>
            </div>
        </main>
    </body>
</html>