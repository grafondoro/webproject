<%-- 
    Document   : restaurant
    Created on : Dec 28, 2016, 5:58:12 PM
    Author     : anase
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Restaurant Page</title>
        <%@include file="base_head.jsp" %>
        <link rel="stylesheet" href="/WebProject/staticfiles/css/carousel.css">
        <link rel="stylesheet" href="/WebProject/staticfiles/css/restaurant.css">
        <script src="/WebProject/staticfiles/js/carousel.js"></script>
        <link rel="stylesheet" href="staticfiles/css/map.css">
        <script>
            function initMap() {
                var pos = {
                    lat: ${restaurant.location.coordinates.latitude},
                    lng: ${restaurant.location.coordinates.longitude}
                };
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 15,
                    center: pos
                });
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map
                });
            }
        </script>
    </head>
    <body>
        <%@include file="base_off_canvas.jsp" %>
        <main class="main-view">
            <%@include file="base_navbar.jsp" %>
            <div class="container">
                <div class="back-button">
                    <a href="/WebProject" class="btn btn-default">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        Torna alla home
                    </a>
                </div>
                <div class="container">
                    <div class="carousel">
                        <c:forEach items="${restaurant.images}" var="image" varStatus = "status">
                            <div class="carousel-photo">
                                <c:if test="${!empty sessionScope.user}">
                                    <c:if test="${sessionScope.user.admin}">
                                        <form action="image?photo_url=${image.link}&id_restaurant=${restaurant.id}&status=1" method="post">
                                            <button class="btn btn-default delete-photo" data-toggle="tooltip" data-placement="bottom" title="Rimuovi">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </button>
                                        </form>
                                    </c:if>
                                    <form action="image?photo_url=${image.link}&id_restaurant=${restaurant.id}&status=2" method="post">
                                        <button class="btn btn-default report-photo" data-toggle="tooltip" data-placement="bottom" title="Segnala">
                                            <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>
                                        </button>
                                    </form>
                                </c:if>
                                <div>
                                    <span class="badge photo-info"/>${image.statusString}</span>
                                </div>
                                <img src="${image.link}" alt="image"/>
                            </div>
                        </c:forEach>
                        <div class="controls">
                            <button class="btn left-arrow">
                                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                                immagine precedente
                            </button>
                            <button class="btn right-arrow">
                                immagine successiva
                                <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <c:if test="${!empty sessionScope.user}">
                        <div class="add-image">
                            <div class="panel panel-default">
                                <div class="panel-heading">Aggiungi immagine</div>
                                <div class="panel-body">
                                    <form action="upload?id_restaurant=${restaurant.id}" class="form-input" method="post" enctype="multipart/form-data">
                                        <input type="file" name="file" />
                                        <input type="submit"/>
                                    </form>
                                </div>
                            </div>
                        </div>            
                    </c:if>
                </div>    
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Info ristorante</div>
                <div class="panel-body">
                    <div class="container">
                        <div class="title">
                            Propietario ristorante
                        </div>
                        <div class="content">
                            <a href="/WebProject/user?id=${restaurant.owner.id}" >${review.reviewer.name} ${review.reviewer.surname}</a>
                        </div>
                    </div>
                    <div class="container">
                        <div class="name">
                            ${restaurant.name}
                        </div>
                        <div class="info">
                            <div class="container">
                                <div class="title">
                                    Sito web
                                </div>
                                <div class="content">
                                    ${restaurant.website}
                                </div>
                            </div>
                            <div class="container">
                                <div class="title">
                                    Prezzi medi    
                                </div>
                                <div class="content">
                                    ${restaurant.average_prices.low} - ${restaurant.average_prices.high}
                                </div>
                            </div>
                            <div class="container">
                                <div class="title">
                                    Cucine
                                </div>
                                <div class="content">
                                    <c:forEach items="${restaurant.cuisines}" var="cuisine">
                                        ${cuisine}
                                    </c:forEach>
                                </div>
                            </div>
                            <div class="container">
                                <div class="title">
                                    Orari di apertura
                                </div>
                                <div class="content">
                                    <c:forEach items="${restaurant.business_hours}" var="opening">
                                        ${opening.day.dayName}: ${opening.opens} - ${opening.closes}
                                        <br>
                                    </c:forEach>
                                </div>
                            </div>
                            <div class="container">
                                <div class="title">
                                    Descrizione
                                </div>
                                <div class="description">
                                    ${restaurant.description}        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <c:if test="${!empty sessionScope.user}">
                        <div class="panel panel-default add-review">
                            <div class="panel-heading">Aggiungi review</div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="post" action="review?id_restaurant=${restaurant.id}">
                                    <div class="form-group">
                                        <textarea class="form-control" name="title" placeholder="Titole" rows="1"></textarea>
                                        <textarea class="form-control" name="content" placeholder="Contenuto" rows="3"></textarea>
                                        <div class="stars">
                                            <c:forEach var="i" begin="1" end="5">
                                                <input class="star" id="star-${i}" type="radio" name="rating" value="${i}"/>
                                                <label class="star" for="star-${i}"></label>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="submit-btn">
                                            <button type="submit" class="btn btn-default">Sign in</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </c:if>
                    <div class="panel panel-default">
                        <div class="panel-heading">Lista review</div>
                        <div class="panel-body reviews">
                            <c:forEach items="${restaurant.reviews}" var="review">
                                <div class="container review">
                                    <div class="reviewer">
                                        <a href="/WebProject/user?id=${review.reviewer.id}" >${review.reviewer.name} ${review.reviewer.surname}</a>
                                    </div>
                                    <div class="actual-review">
                                        <div class="review-title">
                                            <a href="/WebProject/review?id=${review.id}" >${review.title}</a>
                                        </div>
                                        <div class="stars_readonly">
                                            <form action="">
                                                <c:forEach var="i" begin="1" end="5">
                                                    <input class="star_readonly" id="star-${i}-readonly" type="radio" name="star" disabled
                                                           <c:if test="${i == review.rating}">
                                                               checked
                                                           </c:if>
                                                           />
                                                    <label class="star_readonly" for="star-${i}-readonly"></label>
                                                </c:forEach>
                                            </form>
                                            ${review.rating} su 5 stelle
                                        </div>
                                        <div class="review-content">
                                            ${review.content}
                                        </div>
                                        <div class="review-response">
                                            ${review.response}
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <div class="location">
                    <div class="panel panel-default">
                        <div class="panel-heading">Mappa</div>
                        <div class="panel-body">
                            ${restaurant.location.country} > ${restaurant.location.region} > ${restaurant.location.province} > ${restaurant.location.city}
                            <br>
                            <div id="map">

                            </div>
                        </div>
                    </div>
                </div>
                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBylY9u9h55jKQQjtXdvzk-OAvgvhvw7yo&callback=initMap">
                </script>
            </div>
        </main>
    </body>
</html>
