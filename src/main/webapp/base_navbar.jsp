<%-- 
    Document   : layout_navbar
    Created on : Feb 1, 2017, 10:58:46 AM
    Author     : anase
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<nav class="container">
    <div class="form-group nav-search">
        <span class="glyphicon glyphicon-align-justify" id="toggle-off-canvas" aria-hidden="true"></span>
        <input type="text" class="form-control" id="search-input" placeholder="Search">
        <div class="list-group" id="search-result">
        </div>
    </div>
</nav>