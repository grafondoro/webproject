<%-- 
    Document   : search
    Created on : Feb 2, 2017, 5:57:49 PM
    Author     : anase
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="/WebProject/staticfiles/css/restaurant.css">
        <link rel="stylesheet" href="/WebProject/staticfiles/css/list_restaurants.css">
        <%@include file="base_head.jsp" %>
        <script src="staticfiles/js/search.js"></script>
    </head>
    <body>
        <%@include file="base_off_canvas.jsp" %>
        <main class="main-view">a
            <%@include file="base_navbar.jsp" %>
            <div class="parameters container">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li 
                            <c:if test="${sort == 'reputation'}">
                                class="active"
                            </c:if>
                            ><a id="reputation" class="sorting" href="#">Reputazione</a></li>
                        <li 
                            <c:if test="${sort == 'price'}">
                                class="active"
                            </c:if>
                            ><a id="price" class="sorting" href="#">Prezzo</a></li>
                        <li 
                            <c:if test="${sort == 'name' || sort == 'none'}">
                                class="active"
                            </c:if>
                            ><a id="name" class="sorting" href="#">Nome</a></li>
                    </ul>
                </nav>
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <c:forEach var="i" begin="1" end="${pages}">
                            <li 
                                <c:if test="${page == i}">
                                    class="active"
                                </c:if>
                                ><a href="#">${i}</a></li>
                            </c:forEach>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <span hidden>
                    Risultati secondo il criterio <span class="location">${location}</span>
                    <span class="hint">${hint}</span>
                </span>
            </div>
            <c:if test="${!empty restaurants}">
                <div class="claimed-restaurants">
                    <div class="panel panel-default">
                        <div class="panel-heading">Ristoranti utente</div>
                        <div class="panel-body">
                            <c:forEach items="${restaurants}" var="restaurant">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="container list-restaurant">
                                            <div class="list-restaurant-image">
                                                <img class="thumbnail list-restaurant-image" src="${restaurant.cover_image}">
                                            </div>
                                            <div class="list-restaurant-content">
                                                <a href="/WebProject/restaurant?id=${restaurant.id}">${restaurant.name}</a>
                                                <br>
                                                ${restaurant.average_prices.low} € - ${restaurant.average_prices.high} €
                                                <br>
                                                <c:forEach items="${restaurant.reviews}" begin="1" end="3" var="review">
                                                    <a href="/WebProject/review?id=${review.id}" >${review.title}</a> - ${review.rating} su 5 stelle
                                                    <br>
                                                </c:forEach>
                                                ${restaurant.location.region} > ${restaurant.location.province} > ${restaurant.location.city}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </c:if>
        </main>
    </body>
</html>
