    /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function capitalizeFirstLetter(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
}

function validateRepeat(repeat) {
    var retval = false;
    var input_id = "input" + capitalizeFirstLetter(repeat);
    var repeat_input_id = "repeat" + capitalizeFirstLetter(input_id);
    //console.log($("#" + input_id).val());
    //console.log($("#" + repeat_input_id).val());
    /*
    if (!$("#" + repeat_input_id).val() && !$("#" + input_id).val())
        return retval;
    */
    if ($("#" + input_id).val() === $("#" + repeat_input_id).val() && $("#" + input_id).val() !== "") {
        $(".repeat-" + repeat).hide();
        retval = true;
    } else {
        $(".repeat-" + repeat).show();
    }
    return retval;
}

function validateEmail() {
    var retval = true;
    if (isValidEmailAddress($("#inputEmail").val())) {
        $(".email-valid").hide();
        $.getJSON(
                "http://localhost:8080/WebProject/register",
                {
                    email: $("#inputEmail").val()
                },
                function (data) {
                    if (data.duplicate) {
                        $(".email-duplicate").show();
                        retval = false;
                    } else {
                        $(".email-duplicate").hide();
                    }
                }
        );
    } else {
        $(".email-valid").show();
    }
    return retval;
}

function validatePassword() {
    if ($("#inputPassword").val()) {
        $(".password").hide();
        return true;
    } else {
        $(".password").show();
        return false;
    }
}

function validateCheckbox(){
    if ($("#conditions").prop("checked")){
        $(".checkbox-tocheck").hide();
        return false;
    } else {
        $(".checkbox-tocheck").show();
        return true;
    }
}

function validateAll() {
    var retval = true;
    //check email is valid
    if (!validateEmail()) {
        retval = false;
        console.log(retval);
    }
    //check password is valid
    if (!validatePassword()) {
        retval = false;
        console.log(retval);
    }
    //check email and password repeated correctly
    if (!(validateRepeat("email") && validateRepeat("password"))) {
        retval = false;
        console.log(retval);
    }
    if (!($("#inputName").val() && $("#inputSurname").val())) {
        retval = false;
        console.log(retval);
    }
    //check conditions is checked
    if (validateCheckbox()){
        retval = false;
        console.log(retval);
    }
    return retval;
}

$(document).ready(function () {   
    $("#repeatInputEmail").on("input propertychange paste blur", function () {
        if ($("#inputEmail").val()) {
            validateRepeat("email");
        }
    });
    $("#repeatInputPassword").on("input propertychange paste blur", function () {
        if (validatePassword()) {
            validateRepeat("password");
        }
    });
    $("#inputName").blur(function () {
        if ($(this).val()) {
            $(".name").hide();
        } else {
            $(".name").show();
        }
    });
    $("#inputSurname").blur(function () {
        if ($(this).val()) {
            $(".surname").hide();
        } else {
            $(".surname").show();
        }
    });
    $("#inputPassword").on("input propertychange paste blur", function () {
        if ($(this).val()) {
            validatePassword();
            validateRepeat("password");
        }
    });
    $("#inputEmail").blur(function () {
        if ($(this).val()) {
            $(".email-empty").hide();
        } else {
            $(".email-empty").show();
        }
    });
    $("#inputEmail").on("input propertychange paste", function () {
        if ($(this).val()) {
            validateEmail();
        }
    });
       
    $("#submit").on("click keyup", function (e) {
          if (!validateAll()) {
              console.log("qualche errore presente!!");
              e.preventDefault();
          }else{
              console.log("tutto ok!!");
              $("#myForm").submit();
          }
      });
  
});