/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getSuggestions() {
    if ($("#search-input").val()) {
        $.getJSON("/WebProject/suggestion",
                {
                    hint: $("#search-input").val()
                }, function (data) {
            console.log(data);
            $("#search-result").empty();
            $("#search-result").show();
            $.each(data.suggestions, function () {
                var $element = $("<a>").addClass("list-group-item")
                        .text(this.content)
                        .attr("href", this.link);
                $("<span>").addClass("badge")
                        .text(this.description).appendTo($element);
                $("#search-result").append($element);
            });
        });
    } else {
        $("#search-result").hide();
    }
}

function hideSuggestions() {
    $("#search-result").hide();
}

function getNotificationsCount() {
    $.getJSON("/WebProject/notifications/count",
            null,
            function (data) {
                $("#notifications").empty();
                $("#notifications").text("notifiche");
                $("<span>").addClass("badge")
                        .text(data.count).appendTo($("#notifications"));
            });
}

$(document).ready(function () {
    var opened = false;
    $("#toggle-off-canvas").click(function (e) {
        e.stopPropagation();
        if (opened === false) {
            $(".off-canvas, .main-view").animate({
                left: "+=300"
            }, 300, function () {
                opened = true;
            });
        } else {
            $(".off-canvas, .main-view").animate({
                left: "-=300"
            }, 300, function () {
                opened = false;
            });
        }
    });
    $("#search-input").on("keyup focus", $.debounce(300, getSuggestions));
    $("#search-input").blur($.debounce(300, hideSuggestions));
    setInterval(getNotificationsCount, 2000);
});