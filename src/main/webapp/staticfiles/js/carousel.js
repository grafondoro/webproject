/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var images = [];
var image_index = 0;

function hide_image(index) {
    images[Math.abs(index % images.length)].hide();
}

function show_image(index) {
    images[Math.abs(index % images.length)].show();
}

$(document).ready(function () {
    $(".carousel-photo").each(function () {
        images.push($(this));
    });
    show_image(image_index);

    $(".right-arrow").click(function () {
        hide_image(image_index);
        image_index++;
        show_image(image_index);
    });
    $(".left-arrow").click(function () {
        hide_image(image_index);
        image_index--;
        show_image(image_index);
    });
});