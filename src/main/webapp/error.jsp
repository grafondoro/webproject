<%-- 
    Document   : error
    Created on : 1-feb-2017, 17.41.11
    Author     : Nauman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="base_head.jsp" %>
    </head>
    <body>
        <%@include file="base_off_canvas.jsp" %>
        <main class="main-view">
            <%@include file="base_navbar.jsp" %>
            <div class="error-message">
            Ops! Qualcosa è andato storto.
            </div>
            <a class="btn btn-default" href="/WebProject/">Torna alla homepage</a>
        </main>
    </body>
</html>
