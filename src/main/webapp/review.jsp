<%-- 
    Document   : review
    Created on : 1-feb-2017, 16.20.31
    Author     : Nauman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Review Page</title>
        <%@include file="base_head.jsp" %>
        <link rel="stylesheet" href="/WebProject/staticfiles/css/restaurant.css">
    </head>
    <body>
        <%@include file="base_off_canvas.jsp" %>
        <main class="main-view">
            <%@include file="base_navbar.jsp" %>
            <div class="back-button">
                <a href="/WebProject/restaurant?id=${restaurant.id}" class="btn btn-default">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    Torna al ristorante ${restaurant.name}
                </a>
            </div>
            <div class="panel panel-default add-review">
                <div class="panel-heading">Review di ${review.reviewer.name} ${review.reviewer.surname}</div>
                <div class="panel-body">
                    <div class="review-title">
                        <a href="/WebProject/review?id=${review.id}" >${review.title}</a>
                    </div>
                    <div class="review-content">
                        ${review.content}
                    </div>
                    <div class="stars_readonly">
                        <form action="">
                            <c:forEach var="i" begin="1" end="5">
                                <input class="star_readonly" id="star-${i}-readonly" type="radio" name="star" disabled
                                       <c:if test="${i == review.rating}">
                                           checked
                                       </c:if>
                                       />
                                <label class="star_readonly" for="star-${i}-readonly"></label>
                            </c:forEach>
                        </form>
                        ${review.rating} su 5 stelle
                    </div>
                    <c:choose>
                        <c:when test="${!empty review.response}">
                            <div class="review-response">
                                ${review.response}
                            </div>
                        </c:when>
                        <c:when test="${!empty restaurant.owner and (sessionScope.user.id == restaurant.owner.id)}">
                            <form action="/WebProject/response?id_review=${review.id}&id_restaurant=${restaurant.id}" method="post">
                                <textarea class="form-control" name="response" placeholder="Risposta" rows="3"></textarea>
                                <div class="submit-btn">
                                    <button type="submit" class="btn btn-default">Rispondi</button>
                                </div>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <div class="review-response">
                                Nessuna risposta
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </main>
    </body>
</html>
