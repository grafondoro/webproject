<%-- 
    Document   : user
    Created on : Jan 25, 2017, 10:41:42 PM
    Author     : anase
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
    <head>
        <title>${user.name}'s Profile</title>
        <%@include file="base_head.jsp" %>
        <link rel="stylesheet" href="/WebProject/staticfiles/css/carousel.css">
        <link rel="stylesheet" href="/WebProject/staticfiles/css/restaurant.css">
        <link rel="stylesheet" href="/WebProject/staticfiles/css/list_restaurants.css">
        <script src="/WebProject/staticfiles/js/carousel.js"></script>
    </head>
    <body>
        <%@include file="base_off_canvas.jsp" %>
        <main class="main-view">
            <%@include file="base_navbar.jsp" %>
            <div class="panel panel-default">
                <div class="panel-heading">Info utente</div>
                <div class="panel-body">
                    <div class="user-info">
                        <div class="container">
                            <div class="title">
                                Nome e Cognome
                            </div>
                            <div class="content">
                                ${user.name} ${user.surname}
                            </div>
                        </div>
                        <div class="container">
                            <div class="title">
                                Indirizzo email
                            </div>
                            <div class="content">
                                ${user.email}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <c:if test="${!empty restaurants}">
                <div class="claimed-restaurants">
                    <div class="panel panel-default">
                        <div class="panel-heading">Ristoranti utente</div>
                        <div class="panel-body">
                            <c:forEach items="${restaurants}" var="restaurant">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="container list-restaurant">
                                            <div class="list-restaurant-image">
                                                <img class="thumbnail" src="${restaurant.cover_image}">
                                            </div>
                                            <div class="list-restaurant-content">
                                                <a href="/WebProject/restaurant?id=${restaurant.id}">${restaurant.name}</a>
                                                <br>
                                                ${restaurant.average_prices.low} € - ${restaurant.average_prices.high} €
                                                <br>
                                                <c:forEach items="${restaurant.reviews}" begin="1" end="3" var="review">
                                                    <a href="/WebProject/review?id=${review.id}" >${review.title}</a> - ${review.rating} su 5 stelle
                                                    <br>
                                                </c:forEach>
                                                    ${restaurant.location.region} > ${restaurant.location.province} > ${restaurant.location.city}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!empty images}">
                <div class="panel panel-default">
                    <div class="panel-heading">Immagini caricate</div>
                    <div class="panel-body">
                        <div class="container">
                            <div class="carousel">
                                <c:forEach items="${images}" var="image" varStatus = "status">
                                    <div class="carousel-photo">
                                        <div>
                                            <span class="badge photo-info"/>${image.statusString}</span>
                                        </div>
                                        <img src="${image.link}" alt="image"/>
                                    </div>
                                </c:forEach>
                                <div class="controls">
                                    <button class="btn left-arrow">
                                        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                                        immagine precedente
                                    </button>
                                    <button class="btn right-arrow">
                                        immagine successiva
                                        <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
        </main>
    </body>
</html>
