delete from notifications;
delete from users;
delete from restaurants;
delete from cuisines;
delete from business_hours;

alter table users auto_increment = 1;
alter table restaurants auto_increment = 1;
alter table reviews auto_increment = 1;
alter table notifications auto_increment = 1;