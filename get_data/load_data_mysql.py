from get_data.WebProjectUtils import RestaurantsLoader

loader = RestaurantsLoader(load_only=True)

loader.load_restaurants()
