import json
import logging
import re
from asyncio import QueueEmpty
from os import mkdir, walk
from os.path import exists
from queue import Queue
from random import randint, choice, random
from threading import Thread, Lock

import requests
from bs4 import BeautifulSoup
from mysql import connector


class RestaurantsLoader:
    __config = {
        'users_filename': 'users.json',
        'urls_filename': 'urls.txt',
        'users_count': 10 ** 3,
        'concurrent': 10 ** 2
    }

    def __init__(self, load_only=False, **config):

        self.__log = self.__create_log()

        self.__parse_configuration(**config)

        self.__cnx, self.__cur = self.__create_connection()

        if not load_only:
            self.__lock = Lock()
            self.__urls = self.__get_restaurant_list()
            self.__q = Queue(len(self.__urls))

            for u in self.__urls:
                self.__q.put(u)

        self.__empty_db()
        self.__do_users()

    def __parse_configuration(self, **config):
        # Set config dict if is in **config
        for k, v in config.items():
            if k in self.__config:
                self.__config[k] = v
            else:
                raise AttributeError(k)

    @staticmethod
    def __create_connection():
        cnx = connector.connect(user='root', password='root', database='webproject', autocommit=True)
        return cnx, cnx.cursor(buffered=True)

    @staticmethod
    def __create_log():
        if not exists('tmp'):
            mkdir('tmp')

        log = logging.getLogger('trip_advisor_mysql')
        log.setLevel(logging.DEBUG)
        fh = logging.FileHandler('tmp/trip_advisor_mysql.log')
        fh.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s: [%(threadName)s][%(levelname)s]:%(lineno)d -> %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)
        log.addHandler(fh)
        log.addHandler(ch)

        return log

    @staticmethod
    def __get_lines(filename):
        return [a.strip() for a in open(filename).readlines()]

    def get_and_load_restaurants(self):

        concurrent_size = self.__config['concurrent']
        for a in range(concurrent_size):
            t = Thread(target=self.__get_and_load)
            t.daemon = True
            t.start()

        self.__q.join()

    def load_restaurants(self):

        root_dir = 'tmp'

        restaurants = []

        for root, sub_dirs, files in walk(root_dir):
            restaurants = ['{}\\{}'.format(root, f) for f in files if f.endswith('.json')]

        for r in restaurants:
            self.__load_data(r)

    def __do_users(self):
        if exists(self.__config['users_filename']):
            with open(self.__config['users_filename']) as f:
                self.__load_users(json.load(f))
        else:
            self.__get_users()

    def __get_restaurant_list(self):
        if exists(self.__config['urls_filename']):
            with open(self.__config['urls_filename']) as f:
                urls = [u.strip() for u in f.readlines()]
        else:
            urls = self.__get_restaurants()
            with open(self.__config['urls_filename'], 'w') as f:
                f.writelines('\n'.join(urls))
        urls_len = len(urls)

        self.__log.info('found {} restaurants'.format(urls_len))

        return urls

    def __empty_db(self):
        # Apply scripts to empty database and reset auto_increment values
        self.__log.info('Emptying database')
        with open('empty_db.sql') as f:
            li = [l.strip() for l in f.readlines() if len(l.strip()) > 0]

            for stmt in li:
                self.__log.info('Applying {}'.format(stmt))
                self.__cur.execute(stmt)

    def __get_users(self):
        names = self.__get_lines('names.txt')
        surnames = self.__get_lines('surnames.txt')
        domains = self.__get_lines('domains.txt')

        data = dict()

        while len(data) < self.__config['users_count']:
            name = choice(names)
            surname = choice(surnames)
            email = '{}.{}{}@{}.it'.format(name.lower(), surname.lower(), '{:02g}'.format(randint(0, 100)),
                                           choice(domains))
            clear_password = '{}.{}'.format(name, surname)
            user = {
                'name': name,
                'surname': surname,
                'email': email,
                'clear_pwd': clear_password,
                'is_admin': 0
            }

            data[email] = user

        users = [{
            'name': 'Anas',
            'surname': 'El Amraoui',
            'email': 'anas.elamraoui@studenti.unitn.it',
            'clear_pwd': 'admin',
            'is_admin': 1
        }]

        users.extend([d for d in data.values()])

        return self.__load_users(users)

    def __load_users(self, data):
        if not exists(self.__config['users_filename']):
            with open(self.__config['users_filename'], 'w') as f:
                json.dump(data, f, indent=4)

        user_ids = []
        user_query = 'insert into users (name, surname, email, pwd, clear_pwd, is_admin) ' \
                     'values (%(name)s, %(surname)s, %(email)s, password(%(clear_pwd)s), %(clear_pwd)s, %(is_admin)s)'

        for user in data:
            self.__cur.execute(user_query, user)
            user_ids.append(self.__cur.lastrowid)

        self.__log.info('done with users')

    def __get_data(self, restaurant_url):
        restaurant_name = restaurant_url.split('.')[2][3:] + '.json'

        restaurant_filename = 'tmp/' + restaurant_name

        if exists(restaurant_filename):
            self.__log.info("{} already exist! I'm going to load it".format(restaurant_name))
            return restaurant_filename

        resp_restaurant = requests.get(restaurant_url)
        soup_restaurant = BeautifulSoup(resp_restaurant.content, "lxml")

        name = soup_restaurant.find('h1', {'class': 'heading_name'}).text.strip()

        website = 'www.{}.it'.format(name.strip().replace(' ', '_')).lower()

        details_tab = soup_restaurant.find('div', {'class': 'table_section'})

        cuisines = []
        pricing = tuple()
        timing = {}

        for det in details_tab.find_all('div', {'class': 'row'}):
            try:
                title = det.find('div', {'class': 'title'}).text.strip()
                content = det.find('div', {'class': 'content'}).text.strip()

                if title == 'Cucina':
                    cuisines = content.split(',\xa0')
                elif title == 'Prezzi medi':
                    pricing = tuple(re.findall(r'\d+', content))
                elif title == "Orari d'apertura":
                    days = det.find_all('div', {'class': 'detail'})

                    for day in days:
                        day_name = day.find('span', {'class': 'day'}).text.strip()
                        for h in [tuple(b.strip() for b in a.text.split('-')) for a in
                                  day.find_all('div', {'class': 'hoursRange'})]:
                            timing[day_name] = {'open': h[0],
                                                'close': h[1]}

            except Exception as e:
                pass

        if len(timing) == 0:
            self.__log.info('No opening hours')
            return None

        if len(pricing) != 2:
            self.__log.info('No average pricing found for {}'.format(restaurant_url))
            return None

        additional_info = soup_restaurant.find_all('div', {'class': 'additional_info'})

        if len(additional_info) < 2:
            self.__log.info('Not enough additional info')
            return None

        location_info = additional_info[0]

        description = ''

        try:
            description = additional_info[1].find('div', {'class': 'content'}).text.strip()
        except IndexError as e:
            self.__log.info('No description found')
            self.__log.exception(e)
            return None

        try:
            street_address = location_info.find('span', {'class': 'street-address'}).text

            location_attributes = [b.strip() for b in
                                   [a.text for a in soup_restaurant.find_all('div', {'class': 'detail'}) if
                                    a.text.startswith('Local')][0].replace('\xa0>\xa0', '').split('\n')]

            country = location_attributes[2]
            region = location_attributes[3]
            province = location_attributes[4]
            city = location_attributes[5]

            address_maps = 'https://maps.googleapis.com/maps/api/geocode/json'

            payload = {
                'address': '{} {}'.format(city, street_address),
                'language': 'it-IT'
            }

            maps_response = requests.get(address_maps, params=payload)

            maps_json = maps_response.json()

            if maps_json['status'] == 'ZERO_RESULTS':
                self.__log.info('No coordinates found')
                return None

            coordinates = maps_json['results'][0]['geometry']['location']

        except AttributeError as e:
            self.__log.info("Missing location details")
            self.__log.exception(e)
            return None

        img_data = soup_restaurant.find_all('script', text=lambda x: x and 'var lazyImgs' in x)[0]

        js = img_data.text
        js = js.replace("var lazyImgs = ", '')
        js = re.sub(r";\s+var lazyHtml.+", '', js, flags=re.DOTALL)

        images = json.loads(js)

        images_dict = {image['id']: image['data'] for image in images}

        restaurant_images = [image_url for image_url in images_dict.values() if '/media/photo-s/' in image_url]

        reviews_soup = soup_restaurant.find_all('div', {'class': 'basic_review'})

        reviews = []

        for review in reviews_soup:
            rev = {
                'id_restaurant': '',
                'id_user': self.__random_user(),
                'title': review.find('span', {'class': 'noQuotes'}).text.strip(),
                'rating': int(review.find('img', {'class': 'rating_s_fill'}).get('alt').split(' ')[1]),
                'content': review.find('p', {'class', 'partial_entry'}).text.strip()
            }

            reviews.append(rev)

        restaurant = {
            'id_user': '' if random() > .8 else self.__random_user(),
            'name': name,
            'cuisines': cuisines,
            'website': website,
            'pricing': {
                'low': pricing[0],
                'high': pricing[1]
            },
            'opening_times': timing,
            'images': restaurant_images,
            'location': {
                'country': country,
                'region': region,
                'province': province,
                'city': city,
                'street_address': street_address,
                'coordinates': coordinates
            },
            'description': description,
            'reviews': reviews
        }

        with open(restaurant_filename, 'w') as f:
            json.dump(restaurant, f, indent=4)

        return restaurant_filename

    def __random_user(self):
        return randint(1, self.__config['users_count'] - 1)

    def __load_data(self, json_filename):
        self.__log.info("Loading {}".format(json_filename))
        with open(json_filename) as f:
            restaurant_json = json.load(f)

            if restaurant_json['id_user'] == '':
                restaurant_json['id_user'] = None

            location_json = restaurant_json['location']
            price_json = restaurant_json['pricing']
            coordinates_json = location_json['coordinates']

            basic_restaurant = {
                'high_price': price_json['high'],
                'low_price': price_json['low'],
                'lon': coordinates_json['lng'],
                'lat': coordinates_json['lat']
            }

            basic_li = ['name', 'id_user', 'description', 'website']

            for b in basic_li:
                basic_restaurant[b] = restaurant_json[b]

            location_li = ['country', 'region', 'province', 'city', 'street_address']

            for l in location_li:
                basic_restaurant[l] = location_json[l]

            restaurant_query = 'insert into restaurants (name, id_user, low_price, high_price, country,' \
                               'region, province, city, street_address, website, description, longitude, latitude) ' \
                               'values (%(name)s, %(id_user)s, %(low_price)s, %(high_price)s, %(country)s, ' \
                               '%(region)s, %(province)s, %(city)s, %(street_address)s, %(website)s, ' \
                               '%(description)s, %(lon)s, %(lat)s)'

            self.__cur.execute(restaurant_query, basic_restaurant)
            id_restaurant = self.__cur.lastrowid

            for s in restaurant_json['cuisines']:
                cuisine_insert = 'insert ignore into cuisines (id_cuisine) values (%s)'
                self.__cur.execute(cuisine_insert, (s,))

                restaurants_cuisines_insert = 'insert into restaurants_cuisines (id_restaurant, id_cuisine) ' \
                                              'values (%s, %s)'

                self.__cur.execute(restaurants_cuisines_insert, (id_restaurant, s))

            for day, opening in restaurant_json['opening_times'].items():
                business_hours = {
                    'day': day,
                    'opens': opening['open'],
                    'closes': opening['close']
                }

                business_hours_insert = 'insert ignore into business_hours (day, opens, closes) ' \
                                        'values (%(day)s, %(opens)s, %(closes)s)'

                self.__cur.execute(business_hours_insert, business_hours)

                business_hours['id_restaurant'] = id_restaurant

                restaurants_business_hours_insert = 'insert into restaurants_business_hours ' \
                                                    '(id_restaurant, day, opens, closes) ' \
                                                    'values (%(id_restaurant)s, %(day)s, %(opens)s, %(closes)s)'

                self.__cur.execute(restaurants_business_hours_insert, business_hours)

            for i in restaurant_json['images']:
                # 0 -> allowed, 1 -> reported, 2 -> deleted
                random_value = random()
                image_status = 0 if random_value < .8 else 1 if random_value < .9 else 2
                image_insert = 'insert ignore into images (id_restaurant, id_user, photo_url) ' \
                               'values ({}, {}, %s)'.format(id_restaurant, self.__random_user())

                self.__cur.execute(image_insert, (i,))

                if image_status != 0:
                    image = {
                        'photo_url': i,
                        'status': image_status
                    }

                    image_update = 'update images set status = %(status)s ' \
                                   'where photo_url = %(photo_url)s'

                    self.__cur.execute(image_update, image)

            for review in restaurant_json['reviews']:
                try:
                    review['id_restaurant'] = id_restaurant

                    review_insert = 'insert into reviews (id_user, id_restaurant, title, content, rating) ' \
                                    'values (%(id_user)s, %(id_restaurant)s, %(title)s, %(content)s, %(rating)s)'

                    self.__cur.execute(review_insert, review)
                except Exception as e:
                    self.__log.exception(e)

    def __get_restaurants(self):
        url_prefix = 'http://www.tripadvisor.it'

        retval = {}

        with open('regions.txt') as f:
            regions = [(a.split(';')[0], a.split(';')[1]) for a in f.readlines()]

            places = [
                ('Sardegna', 'http://www.tripadvisor.it/Restaurants-g187879-Sardinia.html'),
                ('Molise', 'http://www.tripadvisor.it/Restaurants-g187798-Molise.html')
            ]

            for region in regions:
                region_name = region[0]
                region_url = region[1]
                region_response = requests.get(region_url)
                region_soup = BeautifulSoup(region_response.content, 'lxml')
                self.__log.debug('region -> {}'.format(region_name))
                new_places = [(l.text.split(':')[1].strip(), url_prefix + l.find('a').get('href'))
                              for l in region_soup.find_all('div', {'class': 'geo_name'})]
                places.extend(new_places)
                self.__log.info('found {} places for {}'.format(len(places), region_name))

            for place in places:
                place_url = place[1]
                place_name = place[0]
                place_resp = requests.get(place_url)
                place_soup = BeautifulSoup(place_resp.content, 'lxml')
                self.__log.debug('\tplace -> {}'.format(place_name))

                restaurants = place_soup.find_all('a', {'class': 'property_title'})
                self.__log.info('\tfound {} restaurants for {}'.format(len(restaurants), place_name))
                for restaurant in restaurants:
                    restaurant_name = restaurant.text.strip()
                    restaurant_url = url_prefix + restaurant.get('href')
                    restaurant_key = restaurant_url.partition('Reviews-')[2]
                    self.__log.info("restaurant_key -> {}".format(restaurant_key))
                    retval[restaurant_key] = restaurant_url
                    self.__log.debug('\t\trestaurant -> {}'.format(restaurant_name.encode('utf-8')))

        return list(retval.values())

    def __get_and_load(self):
        while not self.__q.empty():
            try:
                url = self.__q.get_nowait()
                self.__log.info('Getting data for {}'.format(url))
                response_get_data = self.__get_data(url)
                if response_get_data is not None:
                    self.__log.debug('waiting for lock')
                    self.__lock.acquire()
                    self.__log.debug('acquired lock')
                    try:
                        self.__load_data(response_get_data)
                    except Exception as e:
                        self.__log.exception(e)
                        self.__log.critical('failed on {}'.format(response_get_data))
                    self.__lock.release()
                    self.__log.debug('released lock')

                self.__log.info('Remaining {} restaurants to process :)'.format(self.__q.qsize()))
                self.__q.task_done()
            except QueueEmpty as e:
                self.__log.info("Queue empty")
                self.__log.exception(e)
