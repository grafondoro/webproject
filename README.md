# Grafondoro - WebProject #

### What is this repository for? ###

* Webproject for Introduction to Web Programming
* Components
    - Anas El Amraoui - 173767
    - Kesseiji Ugbeyi - 184052
    - Nauman Mahmood - 160368
    - Stefano Zordan - 172461

* Documentation file at ==> https://docs.google.com/document/d/1U39DdFC-huEU_Dwue5RsXtMd499hHXBfNkNdO5R3sE4/edit?usp=sharing


### How do I get set up? ###

* First install complete netbeans and mysql
* Mysql has to have a user named "root" and "root" as password
* Dependencies are managed with maven, if during a build it gives you "missing dependecy",
    use right click on the project within netbeans then click on "build with dependecies".
    Or use the command "mvn clean install" from the same directory of the project,
    for this approach you'll need access to maven from the command line, so make sure you'll
    have it in your enviroment variables.
* For putting all the data into the database use
    1. mysql -u root -proot < structure.sql (this is for creating tables and triggers)
    2. mysql -u root -proot webproject 0< dump.sql (this is for data only, in case you need to reset the dataset)

### Contribution guidelines ###

* NEVER use git pull!! Instead use git fetch origin master + git rebase origin/master.
* Make someone else do the tests
* Always keep trello updated!
* Don't hesitate to create new stories on trello