-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.15-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5146
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for webproject
DROP DATABASE IF EXISTS `webproject`;
CREATE DATABASE IF NOT EXISTS `webproject` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `webproject`;

-- Dumping structure for table webproject.business_hours
DROP TABLE IF EXISTS `business_hours`;
CREATE TABLE IF NOT EXISTS `business_hours` (
  `day` varchar(10) COLLATE utf8_bin NOT NULL,
  `opens` time NOT NULL,
  `closes` time NOT NULL,
  PRIMARY KEY (`day`,`opens`,`closes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table webproject.cuisines
DROP TABLE IF EXISTS `cuisines`;
CREATE TABLE IF NOT EXISTS `cuisines` (
  `id_cuisine` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_cuisine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table webproject.images
DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `photo_url` varchar(100) COLLATE utf8_bin NOT NULL,
  `id_restaurant` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`photo_url`),
  KEY `FK_images_restaurants` (`id_restaurant`),
  KEY `FK_images_users` (`id_user`),
  CONSTRAINT `FK_images_restaurants` FOREIGN KEY (`id_restaurant`) REFERENCES `restaurants` (`id_restaurant`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_images_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table webproject.notifications
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id_notification` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `notification_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` varchar(200) COLLATE utf8_bin NOT NULL,
  `link` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_notification`),
  KEY `FK_notifications_users` (`id_user`),
  KEY `notification_time` (`notification_time`) USING BTREE,
  CONSTRAINT `FK_notifications_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16687 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table webproject.restaurants
DROP TABLE IF EXISTS `restaurants`;
CREATE TABLE IF NOT EXISTS `restaurants` (
  `id_restaurant` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `website` varchar(200) COLLATE utf8_bin NOT NULL,
  `low_price` int(11) NOT NULL,
  `high_price` int(11) NOT NULL,
  `country` varchar(200) COLLATE utf8_bin NOT NULL,
  `region` varchar(200) COLLATE utf8_bin NOT NULL,
  `province` varchar(200) COLLATE utf8_bin NOT NULL,
  `city` varchar(200) COLLATE utf8_bin NOT NULL,
  `street_address` varchar(200) COLLATE utf8_bin NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `description` varchar(4096) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_restaurant`),
  KEY `FK_restaurants_users` (`id_user`),
  CONSTRAINT `FK_restaurants_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1544 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table webproject.restaurants_business_hours
DROP TABLE IF EXISTS `restaurants_business_hours`;
CREATE TABLE IF NOT EXISTS `restaurants_business_hours` (
  `id_restaurant` int(11) NOT NULL,
  `day` varchar(10) COLLATE utf8_bin NOT NULL,
  `opens` time NOT NULL,
  `closes` time NOT NULL,
  PRIMARY KEY (`id_restaurant`,`day`,`opens`,`closes`),
  KEY `FK_restaurants_business_hours` (`day`,`opens`,`closes`),
  CONSTRAINT `FK__business_hours` FOREIGN KEY (`day`, `opens`, `closes`) REFERENCES `business_hours` (`day`, `opens`, `closes`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_business_hours_restaurants` FOREIGN KEY (`id_restaurant`) REFERENCES `restaurants` (`id_restaurant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table webproject.restaurants_cuisines
DROP TABLE IF EXISTS `restaurants_cuisines`;
CREATE TABLE IF NOT EXISTS `restaurants_cuisines` (
  `id_restaurant` int(11) NOT NULL,
  `id_cuisine` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_restaurant`,`id_cuisine`),
  KEY `FK_restaurants_cuisines` (`id_cuisine`),
  CONSTRAINT `FK__cuisines` FOREIGN KEY (`id_cuisine`) REFERENCES `cuisines` (`id_cuisine`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cuisines_restaurants` FOREIGN KEY (`id_restaurant`) REFERENCES `restaurants` (`id_restaurant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table webproject.reviews
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `id_review` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_restaurant` int(11) NOT NULL,
  `title` varchar(4096) COLLATE utf8_bin NOT NULL,
  `content` varchar(4096) COLLATE utf8_bin NOT NULL,
  `response` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id_review`),
  KEY `FK_reviews_users` (`id_user`),
  KEY `FK_reviews_restaurants` (`id_restaurant`),
  CONSTRAINT `FK_reviews_restaurants` FOREIGN KEY (`id_restaurant`) REFERENCES `restaurants` (`id_restaurant`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_reviews_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15035 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for table webproject.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `surname` varchar(50) COLLATE utf8_bin NOT NULL,
  `email` varchar(50) COLLATE utf8_bin NOT NULL,
  `pwd` varchar(50) COLLATE utf8_bin NOT NULL,
  `clear_pwd` varchar(50) COLLATE utf8_bin NOT NULL,
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `last_notification_seen` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.
-- Dumping structure for trigger webproject.images_after_insert
DROP TRIGGER IF EXISTS `images_after_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `images_after_insert` AFTER INSERT ON `images` FOR EACH ROW insert into notifications (id_user, content, link)
	select R.id_user, CONCAT(U.name, ' ', U.surname, ' ha aggiunto una foto!'), CONCAT("/WebProject/restaurant?id=", R.id_restaurant)
	from users U
	natural join images I
	join restaurants R on I.id_restaurant = R.id_restaurant
	where R.id_user <> U.id_user
	and I.photo_url in (select new.photo_url from images)//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger webproject.images_after_update
DROP TRIGGER IF EXISTS `images_after_update`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `images_after_update` AFTER UPDATE ON `images` FOR EACH ROW begin

    insert into notifications (id_user, content, link)
        select U.id_user, concat('Una foto di ', R.name, ' è stata segnalata'), CONCAT("/WebProject/restaurant?id=", R.id_restaurant)
        from (select id_user from users where is_admin = 1) U, images I
        join restaurants R on I.id_restaurant = R.id_restaurant
        where I.status = 1
		  and I.photo_url in (select new.photo_url from images);
        
    insert into notifications (id_user, content, link)
        select I.id_user, concat('Una foto che hai caricato su ', R.name, ' è stata rimossa dal portale'), CONCAT("/WebProject/restaurant?id=", R.id_restaurant)
        from images I
        join restaurants R on I.id_restaurant = R.id_restaurant
        where I.status = 2
		  and I.photo_url in (select new.photo_url from images);
        
end//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger webproject.reviews_after_insert
DROP TRIGGER IF EXISTS `reviews_after_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `reviews_after_insert` AFTER INSERT ON `reviews` FOR EACH ROW insert into notifications (id_user, content, link)
	select Res.id_user, CONCAT(U.name, ' ', U.surname, ' ha aggiunto una recensione al tuo ristorante!'), CONCAT("/WebProject/review?id=", Rev.id_review)
	from reviews Rev
	join restaurants Res on Rev.id_restaurant = Res.id_restaurant
	join users U on U.id_user = Rev.id_user
	where Rev.id_review in (select new.id_review from reviews)
	and Res.id_user is not null//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger webproject.reviews_after_update
DROP TRIGGER IF EXISTS `reviews_after_update`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `reviews_after_update` AFTER UPDATE ON `reviews` FOR EACH ROW insert into notifications (id_user, content, link)
    select Rev.id_user, concat('Il propietario di ', Res.name, ' ha risposto alla tua recensione'), CONCAT("/WebProject/review?id=", Rev.id_review)
    from reviews Rev
    join restaurants Res on Rev.id_restaurant = Res.id_restaurant
    where Rev.id_review in (select new.id_review from reviews)//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
